# Generated by Django 2.2.5 on 2019-10-05 16:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('form', '0003_auto_20191005_1933'),
    ]

    operations = [
        migrations.AlterField(
            model_name='modeljadwal',
            name='kategori',
            field=models.CharField(choices=[('Kelas', 'Kelas'), ('Nugas', 'Nugas'), ('UKOR', 'UKOR'), ('Hangout', 'Hangout'), ('Gabut', 'Gabut')], default='Kelas', max_length=1),
        ),
    ]
