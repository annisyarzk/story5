#views form

from django.shortcuts import render, redirect
from .forms import FormJadwal
from .models import ModelJadwal
from django.core.exceptions import ValidationError


def index(request):
	modelJadwal = ModelJadwal.objects.all()
	args = {
		'hasilJadwal' : modelJadwal,
	}

	return render(request, 'Schedule.html', args)


def create(request):
	formJadwal = FormJadwal(request.POST or None)	# request.POST buat validasi

	# Masukin dari form ke database
	if request.method == 'POST':
		if formJadwal.is_valid():	# Kalau input valid
			formJadwal.save()
			# ModelJadwal.objects.create(
			# 	hari		= formJadwal.cleaned_data.get('hari'),			# Ngambil data yg udah tervalidasi
			# 	waktu		= formJadwal.cleaned_data.get('waktu'),			
			# 	kegiatan	= formJadwal.cleaned_data.get('kegiatan'),	
			# 	kategori	= formJadwal.cleaned_data.get('kategori'),
			# 	tempat		= formJadwal.cleaned_data.get('tempat'),
			# )
			
			return redirect('/schedule')	# Abis ngisi form redirect ke halaman schedule

	args = {
		'formJadwal' : formJadwal,
	}

	return render(request, 'Form.html', args)


def delete(request, delete_id):
	ModelJadwal.objects.filter(id=delete_id).delete()
	return redirect('/schedule')