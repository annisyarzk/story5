from django.db import models
from datetime import datetime
from django.core.exceptions import ValidationError

# def validate_tanggal(value):
# 	try:
# 		input_tanggal = datetime.strptime(value, '%Y/%m/%d')
# 	except ValueError:
# 		raise ValidationError('Incorrect format')

# Kalau pake form dibikin sama kaya forms.py
# Kalau pake modelform bikinnya disini
class ModelJadwal(models.Model):
	HARI = (
		('Monday', 'Monday'),
		('Tuesday', 'Tuesday'),
		('Wednesday', 'Wednesday'),
		('Thrusday', 'Thrusday'),
		('Friday', 'Friday'),
		('Saturday', 'Saturday'),
		('Sunday', 'Sunday'),
	)

	KATEGORI = (
		('Class', 'Class'),
		('Study', 'Study'),
		('Meeting', 'Meeting'),
		('Hangout', 'Hangout'),
		('F5', 'F5'),
	)

	Day 		= models.CharField(max_length=10, choices=HARI, default='Senin')
	Date		= models.DateField()
	Time		= models.TimeField()
	Activity	= models.CharField(max_length=50)
	Place		= models.CharField(max_length=50)
	Category	= models.CharField(max_length=10, choices=KATEGORI, default='Kelas')
	